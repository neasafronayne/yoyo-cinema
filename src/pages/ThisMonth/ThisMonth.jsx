﻿import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from "react-redux";
// Actions
import { fetchMonthMovies } from "../../redux/actions/movies";
// Components
import MoviePosterList from '../../components/MoviePosterList/MoviePosterList';
// Styles
import './ThisMonth.css';

class ThisMonth extends Component {
  componentDidMount() {
    this.props.dispatch(fetchMonthMovies());
  }

  render() {
    const {
      data,
      error,
      loading,
    } = this.props;

    if (error) {
      return <div>Error! {error.message}</div>;
    }

    if (loading) {
      return <div>Loading...</div>;
    }

    return (
      <div className="this-month">
        <MoviePosterList heading="This Month" data={data} />
     </div>
    );
  }
}

ThisMonth.propTypes = {
  data: PropTypes.array,
  loading: PropTypes.bool,
  error: PropTypes.any,
};

ThisMonth.defaultProps = {
  data: [],
  loading: false,
  error: '',
};

const mapStateToProps = state => ({
  data: state.movies.data,
  loading: state.movies.loading,
  error: state.movies.error,
});

export default connect(mapStateToProps)(ThisMonth);
