﻿import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from "react-redux";
// Actions
import { fetchUpcomingMovies } from "../../redux/actions/movies";
// Components
import MoviePosterList from '../../components/MoviePosterList/MoviePosterList';
// Styles
import './Upcoming.css';

class Upcoming extends Component {
  componentDidMount() {
    this.props.dispatch(fetchUpcomingMovies('release_date.asc'));
  }

  render() {
    const {
      upcoming,
      error,
      loading,
    } = this.props;

    if (error) {
      return <div>Error! {error.message}</div>;
    }

    if (loading) {
      return <div>Loading...</div>;
    }

    return (
      <div className="upcoming">
        <MoviePosterList heading="Upcoming Movies in " upcoming data={upcoming} />
     </div>
    );
  }
}

Upcoming.propTypes = {
  movies: PropTypes.array,
  loading: PropTypes.bool,
  error: PropTypes.any,
};

Upcoming.defaultProps = {
  movies: [],
  loading: false,
  error: '',
};

const mapStateToProps = state => ({
  upcoming: state.movies.upcoming,
  loading: state.movies.loading,
  error: state.movies.error,
});

export default connect(mapStateToProps)(Upcoming);
