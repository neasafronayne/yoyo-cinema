﻿import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from "react-redux";
// Actions
import { fetchSearch } from "../../redux/actions/search";
// Components
import MoviePosterList from '../../components/MoviePosterList/MoviePosterList';
// Styles
import './Search.css';

class Search extends Component {
  componentDidMount() {
    const { match: { params: { searchTerm } } } =this.props;
    if (searchTerm) {
      this.props.dispatch(fetchSearch(encodeURI(searchTerm)));
    }
  }

  render() {
    const {
      error,
      loading,
      searchData: {
        results,
      },
      match: { params: { searchTerm } }
    } = this.props;

    if (error) {
      return <div>Error! {error.message}</div>;
    }

    if (loading) {
      return <div>Loading...</div>;
    }

    return (
      <div className="search">
        <MoviePosterList heading="Search for " data={results} searchTerm={searchTerm} />
      </div>
    );
  }
}

MoviePosterList.propTypes = {
  searchData: PropTypes.array,
  loading: PropTypes.bool,
  error: PropTypes.any,
};

MoviePosterList.defaultProps = {
  searchData: [],
  loading: false,
  error: '',
};

const mapStateToProps = state => ({
  searchData: state.search.data,
  loading: state.search.loading,
  error: state.search.error,
});

export default connect(mapStateToProps)(Search);
