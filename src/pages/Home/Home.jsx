﻿import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from "react-redux";
// Actions
import { fetchMonthMovies, fetchUpcomingMovies } from "../../redux/actions/movies";
// Components
import FeatureMovie from '../../components/FeatureMovie/FeatureMovie';
import MoviePosterList from '../../components/MoviePosterList/MoviePosterList';
// Styles
import './Home.css';

class Home extends Component {
  componentDidMount() {
    this.props.dispatch(fetchMonthMovies());
    this.props.dispatch(fetchUpcomingMovies('popularity.desc'));
  }

  render() {
    const {
      data,
      upcoming,
      error,
      loading,
    } = this.props;

    if (error) {
      return <div>Error! {error.message}</div>;
    }

    if (loading) {
      return <div>Loading...</div>;
    }

    return (
      <div className="homepage">
        <h1>
          <span>YoYo</span>, entertainment for the whole family.<br />
          Bringing you crisp, colourful movies.
        </h1>
        <FeatureMovie data={upcoming && upcoming[0]} />
        <MoviePosterList heading="This Month" data={data} />
     </div>
    );
  }
}

Home.propTypes = {
  data: PropTypes.array,
  movies: PropTypes.array,
  loading: PropTypes.bool,
  error: PropTypes.any,
};

Home.defaultProps = {
  data: [],
  movies: [],
  loading: false,
  error: '',
};

const mapStateToProps = state => ({
  data: state.movies.data,
  upcoming: state.movies.upcoming,
  loading: state.movies.loading,
  error: state.movies.error,
});

export default connect(mapStateToProps)(Home);
