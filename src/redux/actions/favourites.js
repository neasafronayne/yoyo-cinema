﻿import * as types from '../types';

// Add Fave movie to list
export const addFavMovie = (newFav) => (dispatch, getState) => {
  const { data } = getState();
  console.log(localStorage.getItem('favState'));
  
  return dispatch({
    type: types.ADD_FAV_SUCCESS,
    payload: { ...data, newFav},
  });
}

// Get List of Popular movies in desc order
export function fetchFavMovies() {
  return dispatch => {
    // dispatch(fetchFavBegin())
    return json => {
      console.log(json);
        dispatch(fetchFavSuccess(json));
        return json;
      };
  };
}

export const fetchFavSuccess = movies => ({
  type: types.FETCH_FAV_SUCCESS,
  payload: { movies },
});
