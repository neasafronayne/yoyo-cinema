﻿import * as types from '../types';
const apiPath = 'https://api.themoviedb.org/3';
const apiKey = 'api_key=4cb1eeab94f45affe2536f2c684a5c9e';
var date = new Date(), y = date.getFullYear(), m = date.getMonth();
var singleMonth = m < 9;
var lastdayDate = function(y,m){
  return  new Date(y, m +1, 0).getDate();
}
var firstDay = y + '-' + (singleMonth ? '0' : '') + (m + 1) + '-01';
var firstDayNextMonth = y + '-' + (singleMonth ? '0' : '') + (m + 2) + '-01';
var lastDay = y + '-' + (m + 1) + '-' + lastdayDate(y,m);
var lastDayYear = y + '-12-' + lastdayDate(y,12);

// Get List of Popular movies in desc order
export function fetchUpcomingMovies(sortBy) {
  return dispatch => {
    dispatch(fetchMoviesBegin());
    return fetch(apiPath + '/discover/movie?sort_by=' + sortBy + '&primary_release_year=' + y + '&region=GB&release_date.gte=' + firstDayNextMonth + '&release_date.lte=' + lastDayYear + '&' + apiKey)
      .then(handleErrors)
      .then(res => res.json())
      .then(json => {
        dispatch(fetchUpcomingMoviesSuccess(json.results));
        return json.results;
      })
      .catch(error => dispatch(fetchMoviesFailure(error)));
  };
}

// Get movies by month
// /discover/movie?primary_release_date.gte=2014-09-15&primary_release_date.lte=2014-10-22
export function fetchMonthMovies() {
  return dispatch => {
    dispatch(fetchMoviesBegin());
    return fetch(apiPath + '/discover/movie?sort_by=primary_release_date.asc&primary_release_year=' + y + '&region=GB&release_date.gte=' + firstDay + '&release_date.lte=' + lastDay + '&' + apiKey)
      .then(handleErrors)
      .then(res => res.json())
      .then(json => {
        dispatch(fetchMoviesSuccess(json.results));
        return json.results;
      })
      .catch(error => dispatch(fetchMoviesFailure(error)));
  };
}

// Handle HTTP errors since fetch won't.
function handleErrors(response) {
  if (!response.ok) {
    throw Error(response.statusText);
  }
  return response;
}

export const fetchMoviesBegin = () => ({
  type: types.FETCH_MOVIES_BEGIN,
});

export const fetchMoviesSuccess = movies => ({
  type: types.FETCH_MOVIES_SUCCESS,
  payload: { movies },
});

export const fetchUpcomingMoviesSuccess = movies => ({
  type: types.FETCH_UPCOMING_MOVIES_SUCCESS,
  payload: { movies },
});

export const fetchMoviesFailure = error => ({
  type: types.FETCH_MOVIES_FAILURE,
  payload: { error },
});
