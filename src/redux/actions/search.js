import * as types from '../types';
const apiPath = 'https://api.themoviedb.org/3';
const apiKey = 'api_key=4cb1eeab94f45affe2536f2c684a5c9e';

// Get List of movies from search term
export function fetchSearch(query) {
  return dispatch => {
    dispatch(fetchSearchBegin());
    return fetch(apiPath + '/search/movie?query=' + query + '&' + apiKey)
      .then(handleErrors)
      .then(res => res.json())
      .then(json => {
        console.log(json);
        dispatch(fetchSearchSuccess(json));
        return json;
      })
      .catch(error => dispatch(fetchSearchFailure(error)));
  };
}

// Handle HTTP errors since fetch won't.
function handleErrors(response) {
  if (!response.ok) {
    throw Error(response.statusText);
  }
  return response;
}

export const fetchSearchBegin = () => ({
  type: types.FETCH_SEARCH_BEGIN,
});

export const fetchSearchSuccess = search => ({
  type: types.FETCH_SEARCH_SUCCESS,
  payload: { search },
});

export const fetchSearchFailure = error => ({
  type: types.FETCH_SEARCH_FAILURE,
  payload: { error },
});
