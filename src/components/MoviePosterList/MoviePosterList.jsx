﻿import React from "react";
import PropTypes from 'prop-types';
// Components
import MoviePoster from '../MoviePoster/MoviePoster';

import './MoviePosterList.css';

// Get current year
var date = new Date(), y = date.getFullYear();


class MoviePosterList extends React.Component {
  render() {
    const {
      data,
      heading,
      favourites,
      upcoming,
      explore,
      searchTerm,
    } = this.props;
    // set heading text
    let headingText = heading;
    if(searchTerm) {
      headingText = <span>{heading} <span>{searchTerm}</span></span>;
    } else if (upcoming) {
        headingText = <span>{heading} <span>{y}</span></span>;
    }

    return (
      <div className="movie-poster-list">
        <div className="list-title">
          <h2>{headingText}</h2>
        </div>
        <div className="movie-list">
          {data && data.length ? data.map(movie => {
            return <MoviePoster favourites={favourites} key={movie.id} data={movie} />
          }) : <div>Sorry no movies to display</div>}
        </div>
      </div>
    );
  }
}

MoviePosterList.propTypes = {
  data: PropTypes.array,
  heading: PropTypes.string,
  upcoming: PropTypes.bool,
  searchTerm: PropTypes.string,
};

MoviePosterList.defaultProps = {
  data: [],
  heading: 'This Month',
  upcoming: false,
  searchTerm: '',
};

export default MoviePosterList;
