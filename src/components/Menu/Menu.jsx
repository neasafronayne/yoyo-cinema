﻿import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
// Styles
import './Menu.css';

const Menu = ({ showMenu }) => (
  <div className={`menu ${showMenu ? 'display' : ''}`}>
    <ul>
      <li><Link to="/this-month">This Month</Link></li>
      <li><Link to="/upcoming">Upcoming</Link></li>
    </ul>
    <div className="contact-info">
      Phone 011 4567 3890 Email <a href="mailto:YoYoCinema.com" title="YoYo Cinema email">YoYoCinema.com</a><br />
      Address Wayne Lane, London SW1 4ES
    </div>
  </div>
);

Menu.propTypes = {
  showMenu: PropTypes.bool,
};

Menu.defaultProps = {
  showMenu: false,
};

export default Menu;