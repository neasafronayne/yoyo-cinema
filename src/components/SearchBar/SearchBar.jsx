import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import './SearchBar.css';

class SearchBar extends Component {
  constructor(props) {
    super(props);

    this.state = { search: ''};
    this.inputChange = this.inputChange.bind(this);
    this.handleClick = this.handleClick.bind(this);
  }

  inputChange(value) {
    this.setState({
      search: value,
    });
  }

  handleClick(value) {
    const { onClick } = this.props;
    onClick();
  }

  render() {
    const { search } = this.state;

    return (
      <div className="search-bar">
        <input
          type="text"
          name="search"
          onChange={e => this.inputChange( e.target.value)}
          placeholder="Search..."
          value={search}
        />
        <Link to={`/search/${search}`} onClick={this.handleClick}>
          <button>Submit</button>
        </Link>
      </div>
    );
  }
}

SearchBar.propTypes = {
  onClick: PropTypes.func,
};

SearchBar.defaultProps = {
  onClick: () => {},
};

export default SearchBar;
